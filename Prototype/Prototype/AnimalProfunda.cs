﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    //Clase prototype profunda para cuando se incluye un objeto dentro de otro
    public class AnimalProfunda : ICloneable
    {
        public int Patas { get; set; }
        public string Nombre { get; set; }
        public Detalles Rasgos { get; set; }

        public object Clone()
        {
            AnimalProfunda clonado = this.MemberwiseClone() as AnimalProfunda;
            Detalles detalles = new Detalles
            {
                Color = this.Rasgos.Color,
                Raza = this.Rasgos.Raza
            };
            clonado.Rasgos = detalles;
            return clonado;
        }   
    }
    //
    public class Detalles
    {
        public string Color { get; set; }
        public string Raza { get; set; }
    }//
}//