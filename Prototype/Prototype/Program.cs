﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            //Implementación de prototype superficial
            ImplementacionPrototypeSuperficial();
            //Implementación de prototype profunda
            ImplementacionPrototypeProfunda();
        }
        //
        private static void ImplementacionPrototypeSuperficial()
        {
            //Implementación de prototype superficial
            AnimalSuperficial oAnimal = new AnimalSuperficial() { Nombre = "Oveja Dolly", Patas = 4 };
            AnimalSuperficial oAnimalClonado = oAnimal.Clone() as AnimalSuperficial;
            oAnimalClonado.Patas = 5;

            Console.WriteLine(oAnimal.Patas);
        }
        //
        private static void ImplementacionPrototypeProfunda()
        {
            //Implementación de prototype profunda
            AnimalProfunda oAnimal = new AnimalProfunda() { Nombre = "Oveja Dolly", Patas = 4 };
            oAnimal.Rasgos = new Detalles
            {
                Color = "Blanca",
                Raza = "Oveja"
            };

            AnimalProfunda oAnimalClonado = oAnimal.Clone() as AnimalProfunda;
            oAnimalClonado.Rasgos.Color = "Negro";
            oAnimalClonado.Nombre = "Oveja negra";

            Console.WriteLine("animal original: " + oAnimal.Rasgos.Color);
            Console.WriteLine("animal clonado: " + oAnimalClonado.Rasgos.Color);
            Console.WriteLine("animal original: " + oAnimal.Nombre);
            Console.WriteLine("animal clonado: " + oAnimalClonado.Nombre);
        }

    }//END CLASS
}//END NAMESPACE